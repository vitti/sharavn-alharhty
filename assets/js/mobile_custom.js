//Fullscreen apps issues
startIn = function() {
    // Send focus to page as it is now display: block
    $.mobile.focusPage($to);

    // Set to page height
    $to.height(screenHeight + toScroll);

    scrollPage();

    if (!none) {
        $to.animationComplete(doneIn);
    }

    $to.addClass(name + " in" + reverseClass);
    $to.addClass($.mobile.activePageClass);
    if (none) {
        doneIn();
    }
};
