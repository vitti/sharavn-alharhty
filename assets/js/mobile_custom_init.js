
    $(document).bind("mobileinit", function(){
          $.mobile.defaultPageTransition = 'slide';
	});

	$(document).ready(function () { 
		// REmoveing the content from page - make data-cache to work.
	    $('div').on('pagehide', function (event, ui) {
	        var $this = $(this);
	        if ($this.attr('ID') !== undefined && $this.attr('data-cache') !== undefined && $this.attr('data-cache') == "never") {
	            var page = $this.attr('ID');
	            $(document.getElementById(page)).remove();
	        }
	    });

	    // Add userAgents
		(navigator.userAgent.match(/Android/i)) 
		? $('html').addClass('androind')
		: (navigator.userAgent.match(/webOS/i))
		? $('html').addClass('webOS')
		: (navigator.userAgent.match(/iPhone/i))
		? $('html').addClass('iPhone')
		: (navigator.userAgent.match(/iPad/i))
		? $('html').addClass('iPad')
		: $('html').addClass('Others');

		// For collapsible      
	    $('body').on('click', 'div.collapsible a.prodCat', function(e){e.preventDefault();
	    	var _active = $(this).hasClass('active'), _prod = $(this).next();
	    	if(!_active){
	    		_prod.stop().slideDown();
	    		$(this).addClass('active');
	    	}else{
	    		_prod.stop().slideUp();
	    		$(this).removeClass('active');
	    	}
	    });


	});

	$(document).on('pageshow', function(){		

			if($("#store").length){

				initialize();
                                calculateDistances();

		};
			
		// Sliders
		var sliders, _auto = ($('#home').length) ? true : false;
		slider = $('.sliders').bxSlider({
			mode: 'horizontal',
			adaptiveHeight: true,
			useCSS: true,
			auto: _auto,
			controls: true
			});   
		

	    //QTY Field to get back in position after keyboard
	    $('input.qty').on('blur', function(){
	    	$('html, body').stop().animate({ scrollTop : 0 }, 500);
	    })

	});


	// $('body').on('touchmove', function(e) {
	//     e.preventDefault();
	// });	
	touchMove = function(event) {
        event.preventDefault();
    }

var parentId=3;
var level=2;
var pId;
var cateId;
var cateName;
var serachString;
var ip;




 /*Script for home page*/

  /*  $(document).delegate("#home","pageshow",function(){
                  
                         $.mobile.showPageLoadingMsg();
                         
                         $.ajax({
                                
                                cache: false,
                                url:"http://kit.jit.su/vmdetails?vmname=vittimgr&restype=minimal&contenttype=json",
                                type: "GET",
                                data: "{}",
                                dataType: "json",
                                error: ajaxCallFailed,
                                success: handleResponseSuccessHome
                                
                        });  
            
    });

    handleResponseSuccessHome= function(data){
      
        ip=data.IP;
        window.sessionStorage.setItem('ip', ip);

        ipCheck();
        //$.mobile.hidePageLoadingMsg();
    
    };

    function ajaxCallFailed(){
    
        window.location = "Error.html";
        return false;
    
    
    }

    function ipCheck(){
    
        var newIp = sessionStorage.getItem("ip");
    
        $.ajax({
           
               cache: false,
               url:"http://"+newIp+"/magento_demo/custom_php/update_database.php?ReceiveIp="+newIp,
               type: "GET",
               data: "{}",
               dataType: "json",
               error: ajaxCallFailedIpCheck,
               success: handleResponseSuccessIpCheck
           
           });
    
    
    
    
    }
    handleResponseSuccessIpCheck= function(data){
    
       
        $.mobile.hidePageLoadingMsg();
    
    };
    function ajaxCallFailedIpCheck(){
    
        window.location = "Error.html";
        return false;
    
        
    }   */

    //ip = sessionStorage.getItem("ip");
    ip="122.164.8.41";

	/* Script for Product  category page*/

	$(document).delegate("#prodCat","pageshow",function(){

	    $("#products1").empty();
	    $.mobile.showPageLoadingMsg();	             	 
	    $.ajax({
			 type : "GET",
			 url: "http://"+ip+"/magento_demo/custom_php/category_jsonp.php?parentid="+parentId+"&level="+level,
			 dataType: "jsonp",
			 contentType : "application/javascript",
			 jsonpCallback : "handleResponse",
             		error: ajaxCallFailedCategory,
			 success: handleResponseProdCatSuccess				
		}); 
	});
handleResponseProdCatSuccess= function(data){
		 
	 var html="";
	html+='<div class="collapsible">';		
	 for(var i=0;i<data.length;i++){	 
		html+= '<a href="#" class="prodCat" data-role="button" data-mini="true" name="'+data[i].name+'"  ccount='+data[i].children_count+' level='+data[i].level+' id='+data[i].entity_id+' onclick="getProducts(this);" ><center>' + data[i].name+ '</center></a><div id='+data[i].entity_id+'pd'+'></div></br>';
			
	  }
	html+='</div>';

	 $("#products1").append($(html));
	 $("#products1").trigger('create');
         
     $.mobile.hidePageLoadingMsg();  

       			
};

function ajaxCallFailedCategory(){
    alert("error");
}
	
		
function getProducts(value){
	 
	pId = $(value).attr('id');
	var level = $(value).attr('level');
	var ccount = $(value).attr('ccount');
	var catname = $(value).attr('name');

	level=++level;

	if(ccount>0){
		
		$.mobile.showPageLoadingMsg();
		
		$.ajax({
			 cache : false,
			 type : "GET",
			 url: "http://"+ip+"/magento_demo/custom_php/category_jsonp.php?parentid="+pId+"&level="+level,
			 dataType: "jsonp",
			 contentType : "application/javascript",
			 jsonpCallback : "handleResponse",
                         error: ajaxCallFailed,
			 success: handleResponseSucSubCat
				
		});
		
	}else{
		
	   window.sessionStorage.setItem('categoryId', pId);
	   window.sessionStorage.setItem('categoryName', catname);
	   //window.location = "product-listing.html";
        $.mobile.changePage( "product-listing.html", { transition: "slide"} );
	 }

}

 
handleResponseSucSubCat= function(data){	
	
	var html="";
    for(var i=0;i<data.length;i++){
			
		html+= '<div class="prods1"><a href="#" class="prodCat" data-role="button" data-mini="true" name="'+data[i].name+'"  ccount='+data[i].children_count+' level='+data[i].level+' id='+data[i].entity_id+' onclick="getProducts(this);">' + data[i].name+ '</a><div id='+data[i].entity_id+'pd'+'></div></div>';
				
	}  

	 var prodDisplayDivvId=pId+"pd";

     document.getElementById(prodDisplayDivvId).innerHTML=html;
	 $("#prodDisplayDivvId").trigger('create');     

	 $.mobile.hidePageLoadingMsg();
  
}; 

/* script for Product Display */

	$(document).delegate("#productList","pageshow",function(){
   	    $.mobile.showPageLoadingMsg();
		cateId = sessionStorage.getItem("categoryId");
   		cateName = sessionStorage.getItem("categoryName");
  
 		$.ajax({
	
		       cache : false,
		       type : "GET",
		       url: "http://"+ip+"/magento_demo/custom_php/category_id2jsonp.php?varname="+cateId,
		       dataType: "jsonp",
		       contentType : "application/javascript",
     			jsonpCallback : "handleResponse",
                        error: ajaxCallFailed,
       		       success: handleResponseSuccessProdDisplay,

   });

	});

handleResponseSuccessProdDisplay= function(data){
    
    
    
    var html ='<ul data-role="listview">';
    
    if(data==""){
        html+='<h1 style="color:white;margin-top:20%;font-size:30px;">Sorry....No products found</h1>';
    }
    else{
        for (var key in data) {
        
            var value = data[key];

            html+='<li><a href="#" class="productclass" prodid='+value.entity_id+'><img class="ul-li-thumb" src='+value.image_url+' alt="No Image"/>';
            html += '<h5 style="align:left;color:#444" class="productclass" prodid ='+value.entity_id+'>' + value.name+ '</h5>';
            html += '<h5 style="align:left;display:inline;color:#444" class="productclass" prodid ='+value.entity_id+'> Price&nbsp:&nbsp<h4 style="color:#900; display:inline;">$' + value.final_price_with_tax+ '</h4></h5>';
            html+='<h5 style="padding-right:1%;"  class="productclass" prodid ='+value.entity_id+'  id='+value.entity_id+'div'+'></h5></a></li>';
        
        }
    }
    html+='</ul>';

    document.getElementById("prodName").innerHTML= cateName;

    
    
  $("#prodDisplay").append($(html)); 
  $("#prodDisplay").trigger('create'); 
  
  ratingDisplay();

  $.mobile.hidePageLoadingMsg();
  
  
  $("#prodDisplay").listview('refresh');
};



function ratingDisplay(){
    
    $.ajax({

           cache : false,
           type : "GET",
           url: "http://"+ip+"/magento_demo/custom_php/rating_on_products_jsonp.php?categoryid="+cateId,
           dataType: "jsonp",
           contentType : "application/javascript",
           jsonpCallback : "handleResponse",
           error: ajaxCallFailed,
           success: handleResponseSuccessRateDisplay         

        });
    
    
}

handleResponseSuccessRateDisplay=function(data){
    
    
    
    for (var key in data) {
        
        var template="";
        var value = data[key];
        var targetDiv=value.product_id+"div";
        var rating=value.rating;

        for(var i=0;i<rating;i++){
            
            template+='<h5 style="display:inline;padding-right:1%;"><img src="assets/images/star_red.png" height="18px"></h5>';
        }

	for(var i=0;i<5-rating;i++){
            
            template+='<h5 style="display:inline;padding-right:1%;"><img src="assets/images/star_white.jpg" height="18px"></h5>';
        }
        
        document.getElementById(targetDiv).innerHTML=template;
        
    }
    
};



$(document).delegate('.productclass', 'click',function(){ 
    var productId = $(this).attr('prodid');

    window.sessionStorage.setItem('prodId', productId);

     $.mobile.changePage( "product.html", { transition: "slide"} );
    return false;

});


	/* Script for product.html page*/

	$(document).delegate("#products","pageshow",function(){
   		$.mobile.showPageLoadingMsg();
		var productId= sessionStorage.getItem("prodId");

			
		 $.ajax({
			 
			 cache : false,
			 type : "GET",
			 url: "http://"+ip+"/magento_demo/custom_php/product_id2jsonp.php?varname="+productId,
			 dataType: "jsonp",
			 contentType : "application/javascript",
			 jsonpCallback : "handleResponse",
			 error : ajaxCallFailedProduct,
			 success: handleResponseSuccessNikon

		
			}); 

		
	});


	handleResponseSuccessNikon= function(data){	
	
		/*$('#prodDesc').attr('href', data.entity_id);
		$('#addInfo').attr('href', data.entity_id);
		$('#custRevw').attr('href', data.entity_id);
		$('#custRevw').attr('price', data.final_price_with_tax);
		$('#custRevw').attr('short', data.short_description);*/	

		 var prodName=data.name;						 
		 var price ='<h4 style="color:#fff;display:inline;">Price:</h4><h4 style="display:inline; color:#fff;"> $'+data.final_price_with_tax+'</h4>';
		 var shortDesc='<p>'+data.short_description+'</p><br>';		 

 		 document.getElementById("pricePlace").innerHTML=price;
		 document.getElementById("productName").innerHTML= prodName; 
			
		ratingOnProductPage(data.entity_id);		
		
		document.getElementById("prodShortDesc").innerHTML=shortDesc;	
		 window.sessionStorage.setItem('shortDescription', shortDesc);
        var price1='<h4 style="color:#111;display:inline;">Price:</h4><h4 style="display:inline; color:#900;"> $'+data.final_price_with_tax+'</h4>';
		 window.sessionStorage.setItem('Price', price1);
	};

	function ajaxCallFailedProduct(){

		alert("fail");
	}


	function ratingOnProductPage(productId){        		
    			
	    $.ajax({
		   
		   cache : false,
		   type : "GET",
		   url: "http://"+ip+"/magento_demo/custom_php/rating_for_search_jsonp.php?productid="+productId,
		   dataType: "jsonp",
		   contentType : "application/javascript",
		   jsonpCallback : "handleResponse",
		   error : ajaxCallFailedRateOnProduct,
		   success: handleResponseSuccessRateDisplayOnProduct		   
	   });	    
		
	    
	 }

	handleResponseSuccessRateDisplayOnProduct=function(data){
	
	    var template="";
	    var rating=data.type.search_rating;
	    var productId=data.msg;	    
	    
	    for(var i=0;i<rating;i++){
		
		template+='<img style="display:inline;padding-left:1%;" src="assets/images/star_red.png" >';
	    }
	    
	    for(var i=0;i<5-rating;i++){
		
		template+='<img style="display:inline;padding-left:1%;" src="assets/images/star_white.jpg">';
	    }
	    
	    document.getElementById("ratingStar").innerHTML=template;
		
		//Slider();		
		imageSlide(productId);			
	};

	function ajaxCallFailedRateOnProduct(){

		 alert("rate error");	    
		// window.location = "Error.html";
	        // return false;
	};


	function imageSlide(productId){    
    		
    	 $.ajax({		   
		   cache : false,
		   type : "GET",
		   url: "http://"+ip+"/magento_demo/custom_php/home_jsonp.php?prodimg="+productId,
		   dataType: "jsonp",
		   contentType : "application/javascript",
		   jsonpCallback : "handleResponse",
		   error : ajaxCallFailedInImageSlide,
		   success: handleResponseSuccessInImageSlide
		   
	   });
	    
	    
	 }

	handleResponseSuccessInImageSlide=function(data){
	    
			//alert("imghand");
		var template='<div class="prodSlider sliders">';
	    
		 $.grep(data, function(obj, key){

			template += '<div class="slide"><div class="prodImage"><img src=http://192.168.1.104/magento/media/catalog/product'+obj.value+'></div></div>';

		 });
		
		template+='</div>';		
		document.getElementById("imagePlace").innerHTML=template; 
		 
	         Slider(); 		
		
	};

	function ajaxCallFailedInImageSlide(){   
		alert("imageerror");
	   // window.location = "Error.html";
	    //return false;
	};

	function Slider(){
		//alert(document.getElementById("imagePlace").innerHTML);
		var sliders, _auto = ($('#products').length) ? true : false;
		slider = $('.sliders').bxSlider({
			mode: 'horizontal',
			adaptiveHeight: true,
			useCSS: true,
			auto: _auto,
			controls: true
			});  

		$.mobile.hidePageLoadingMsg();   
	}

/* Script for Product Description page */

	$(document).delegate("#prodDescription","pageshow",function(){
	$.mobile.showPageLoadingMsg();
 	var productId= sessionStorage.getItem("prodId");

	 $.ajax({
		cache:false,
		 type : "GET",
		 url: "http://"+ip+"/magento_demo/custom_php/product_id2jsonp.php?varname="+productId,
		 dataType: "jsonp",
		 contentType : "application/javascript",
		 jsonpCallback : "handleResponse",
                 error: ajaxCallFailed,
		 success: handleResponseSuccessProdDesc

		}); 
	});

handleResponseSuccessProdDesc= function(data){
	

    document.getElementById("prodName2").innerHTML= data.name;
	 var shortDesc='<p style="padding:0 10px;">'+data.short_description+'</p></br><center><h3 class="price" style="display:inline;;">Price: $<h4 style="color:#900;display:inline;">'+data.final_price_with_tax+'</h4></h3></center>';	 
	 var mainDesc = '<p>'+data.description+'</p>';
	 $("#shortDesc").append($(shortDesc));	 
	 $("#shortDesc").trigger('create'); 	 
     $("#mainDesc").append($(mainDesc));	 
	 $("#mainDesc").trigger('create');  

	 $.mobile.hidePageLoadingMsg(); 
	 $("#mainDesc").listview('refresh');

	//window.sessionStorage.setItem('prodId', productId);	
};

/* Script for Additional Info page */

	$(document).delegate("#additional_info","pageshow",function(){
	$.mobile.showPageLoadingMsg();
	
 	var productId = sessionStorage.getItem("prodId");
	
	 $.ajax({
		 
		 cache : false,
		 type : "GET",
		 url: "http://"+ip+"/magento_demo/custom_php/product_id2jsonp.php?varname="+productId,
		 dataType: "jsonp",
		 contentType : "application/javascript",
		 jsonpCallback : "handleResponse",
        error: ajaxCallFailed,
		 success: handleResponseAddInfo
		
		});

	});

handleResponseAddInfo= function(data){
	
    document.getElementById("prodName1").innerHTML= data.name;
		var shortDesc1='<p style="padding:0 10px;">'+data.short_description+'</p></br><center><h3 class="price" style="display:inline;;">Price: $<h4 style="color:#900;display:inline;">'+data.final_price_with_tax+'</h4></h3></center>';	 
		var inDepth = '<center><h5>'+data.in_depth+'</h5></center>';
		var mp='<center><h5>'+data.megapixels+'</h5></center>';
		var dimen='<center><h5>'+data.dimension+'</h5></center>';
		var model='<center><h5>'+data.model+'</h5></center>';
		 
	 	$("#shortDesc1").append($(shortDesc1));	 
	 	$("#shortDesc1").trigger('create');	 	
	 	$("#inDepth").append($(inDepth));		 
        $("#inDepth").trigger('create');	
	 	$("#mp").append($(mp));		 
	 	$("#mp").trigger('create');	 	
	 	$("#dimen").append($(dimen));		 
	 	$("#dimen").trigger('create');	 	
	 	$("#model").append($(model));
	 	$("#model").trigger('create');
	   
    //document.getElementById("model").innerHTML=model;
	 	
        $.mobile.hidePageLoadingMsg(); 
        $("#model").listview('refresh');
    
    	
};

/* Script for Customer Review Page */


	$(document).delegate("#customerReview","pageshow",function(){
		$.mobile.showPageLoadingMsg();
	var productId = sessionStorage.getItem("prodId");

	$.mobile.showPageLoadingMsg(); 

 		$.ajax({

           cache : false,
           type : "GET",
           url:"http://"+ip+"/magento_demo/custom_php/mysql_rating_jsonp.php?productid="+productId,
           dataType: "jsonp",
           contentType : "application/javascript",
           jsonpCallback : "handleResponse",
           error: ajaxCallFailed,
           success: handleResponseSuccessCustRevw
		});
	});

handleResponseSuccessCustRevw=function(data){
	var shortDesc = sessionStorage.getItem("shortDescription"); 
	var price = sessionStorage.getItem("Price");
    var template='<div class="prodSlider sliders">';
	    
		 /*$.grep(data, function(obj, key){

			template += '<div class="slide"><div class="revHd"><span class="revTitle">'+obj.title+'</span>-Review By <span class="revBy">'+obj.nickname+'</span></div><br /><ul class="revSpec"><li><span class="revName">Quality</span><span class="revstars">: STARS</span></li><li><span class="revName">Value</span><span class="revstars">: STARS</span></li><li><span class="revName">Price</span><span class="revstars">: STARS</span></li></ul><div class="recDtls" id="detail">'+obj.detail+'</div></div>';

		 });
		
		template+='</div>';*/

        var i="";
	
		if(data.number_of_review==0){
		
			template+='<p>No Reviews Avaliable...</p>';
		
		}
	
		$.grep(data.ratings, function(obj, key){
			
			
			template += '<li> <div class="content"><strong>'+obj.title+'</strong>&nbsp<h4 style="font-size:10px; display:inline;">-Review by '+obj.nickname+'</h4><br /> ';

			
			template+='<br /><h4 style="display:inline;font-size:14px;padding-right:1em;">Quality</h4><p style="display:inline; padding-right:1em;">:</p>';
			
			for(i=0;i<obj.Quality;i++){
				
				template+='<img  style="display:inline;padding-left:1%;" src="assets/images/star_red.png">';
			}
			for(var i=0;i<5-obj.Quality;i++){
				
				template+='<img  style="display:inline;padding-left:1%;" src="assets/images/star_white.jpg">';
			}
			template+='<br /><h5 style="display:inline;font-size:14px;padding-right:1.8em;">Value</h5><p style="display:inline; padding-right:1em;">:</p>';
			
			for(i=0;i<obj.Value;i++){
				
				template+='<img style="display:inline;padding-left:1%;" src="assets/images/star_red.png">';
			}
			for(var i=0;i<5-obj.Value;i++){
				
				template+='<img style="display:inline;padding-left:1%;" src="assets/images/star_white.jpg">';
			}
			template+='<br /><h5 style="display:inline;font-size:14px;padding-right:1.9em;">Price</h5><p style="display:inline; padding-right:1em;">:</p>';
			for(i=0;i<obj.Price;i++){
	
				template+='<img style="display:inline; padding-left:1%;" src="assets/images/star_red.png">';
			}
			for(var i=0;i<5-obj.Price;i++){
				
				template+='<img style="display:inline;padding-left:1%;" src="assets/images/star_white.jpg">';
			}
			template+='<p>'+obj.detail+'</p></div> </li>';
		});
		


		
    	 
		var html="";
		html+='<p style="padding:0 10px;">'+shortDesc+'</p>';
		html+='<center><h3 class="price">'+price+'</h3></center>';
		document.getElementById("shortDesc").innerHTML=html;
		document.getElementById("review").innerHTML=template; 
		 
	         Slider(); 		
		
	};


	function Slider(){
		//alert(document.getElementById("imagePlace").innerHTML);
		var sliders, _auto = ($('#customerReview').length) ? true : false;
		slider = $('.sliders').bxSlider({
			mode: 'horizontal',
			adaptiveHeight: true,
			useCSS: true,
			auto: _auto,
			controls: true
			});  

		$.mobile.hidePageLoadingMsg();   
	}

/* script for Search results page*/



	function searchFunction(){

		var value=document.forms[0].searchval.value;
        if(value==""){
            alert("please enter some keyword to search");
           
            value="";
            window.sessionStorage.setItem('searchValue', value);
            
        }
        else{
            window.sessionStorage.setItem('searchValue', value);
            }
	}


    var html;

    $(document).delegate("#searchList","pageshow",function(){    
   
    var searchString = sessionStorage.getItem("searchValue");
                         if(searchString==""){
                            $.mobile.showPageLoadingMsg();
                            html='<h1 style="color:white;">Please enter a keyword to search.</h1>';
                         
                            $("#searchResult").append($(html));
                            $("#searchResult").trigger('create');
                         
                            $.mobile.hidePageLoadingMsg();
                         }
                         else{
                            $.mobile.showPageLoadingMsg();
                            var restfulWebServiceURI;
                            restfulWebServiceURI ="http://"+ip+"/magento_demo/custom_php/solarsearch_jsonp.php?search="+ searchString;
                            $.ajax({
                                   url: restfulWebServiceURI,
                                   data: "{}",
                                   type: "POST",
                                   dataType: "jsonp",
                                   contentType : "application/javascript",
                                   jsonpCallback : "handleResponse",
                                   error: ajaxCallFailed,
                                   success: handleResponseSuccessSearch
                            });
                         }
        });


    handleResponseSuccessSearch= function(data){
        if(data.response.docs.length==0){
        html='<h1 style="color:white;">Sorry....No matched products found</h1>';
        
        $("#searchResult").append($(html)); 
        $("#searchResult").trigger('create');  

        $.mobile.hidePageLoadingMsg();   
        $("#searchResult").listview('refresh');

          }else{

        html ='<ul data-role="listview">';
         
        for ( var i = 0; i < data.response.docs.length; i++){

        html+='<li><a href="#" class="productclass"  prodid ='+data.response.docs[i].id+'><img  alt="No Image" src=http://192.168.1.104/magento/media/catalog/product'+data.response.docs[i].image_magento +' />';
        html += '<h3  class="productclass"  prodid ='+data.response.docs[i].id+'>' + data.response.docs[i].prod_name+ '</h3>';
        html += '<p class="searchPrice" prodid ='+data.response.docs[i].id+'> Price:<span> $' + data.response.docs[i].price_magento+ '</span></p>';
                
                
                var ratingSearch=data.response.docs[i].rating_magento;            
                searchRateDisplay(ratingSearch,data.response.docs[i].id);

                
        html+='</a></li>';
        }

        html+='</ul>';


        $("#searchResult").append($(html)); 
        $("#searchResult").trigger('create');  
        $.mobile.hidePageLoadingMsg();    
        $("#searchResult").listview('refresh');

        }
    }

    function searchRateDisplay(searchRating,productId){

        if(searchRating>0){
        
        html+='<p class="searchRating">';
        
        for(var i=1;i<=searchRating;i++){
         
         html+='<h5 style="display:inline;padding-right:1%;" class="productclass" prodid='+productId+'><img src="assets/images/star_red.png" height="18px"></h5>';
         }
         
         for(var i=1;i<=5-searchRating;i++){
         
         html+='<h5 style="display:inline;padding-right:1%;" class="productclass" prodid='+productId+'><img src="assets/images/star_white.jpg" height="18px"></h5>';
         }
        html+='</p>';
        }

    }






/*script for store page*/

		
                    var myLat;
                    var myLong;
                    
                    var map;
                    var geocoder;
                    var bounds = new google.maps.LatLngBounds();
                    var markersArray = [];
                    var origin1;
                   origin1 = new google.maps.LatLng(13.04271, 80.24751);
                    
                    var destinationA = 'Hyderabad, India';
                    var destinationB = 'Banglore, India';
                    var destinationC = 'Delhi, India';
                    var destinationD = 'Culcutta, India';
                    var destinationE = 'Mumbai, India';
                    var destinationF = 'Punjab, India';
                    var destinationG = 'Vishakapatnam, India';
                    
                    var destinationIcon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=D|FF0000|000000';
                    var originIcon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=O|FFFF00|000000';
                    
                    // function onLoad(){

                   /* $(document).on('pageshow','#store', function() {
                                         
						alert($("#store").length);
                                    //document.addEventListener("deviceready", onDeviceReady, false);
                                        // initialize();
                                        // calculateDistances();
                                         
                        });*/
                    // }
                    
                    function onDeviceReady() {
                        
                        navigator.geolocation.getCurrentPosition(onSuccess, onError);
                        
                    }
                    
                    //GEOLOCATION
                    var onSuccess = function(position) {
                        
                        myLat = position.coords.latitude;
                        myLong = position.coords.longitude;
                        origin1 = new google.maps.LatLng(myLat, myLong);
                        initialize();
                        calculateDistances();
                        
                    };
                    
                    function onError(error) {
                        
                        alert("error");
                        alert("code: "    + error.code    + "\n" +
                              "message: " + error.message + "\n");
                    }
                    
                    function initialize(){
                        
                        $.mobile.showPageLoadingMsg();
                        geocoder = new google.maps.Geocoder();
                        
                    }
                    
                    function calculateDistances() {
                        var service = new google.maps.DistanceMatrixService();
                        service.getDistanceMatrix(
                                                  {
                                                  origins: [origin1],
                                                  destinations: [destinationA,destinationB,destinationC,destinationD,destinationE,destinationF,destinationG],
                                                  travelMode: google.maps.TravelMode.DRIVING,
                                                  unitSystem: google.maps.UnitSystem.METRIC,
                                                  avoidHighways: false,
                                                  avoidTolls: false
                                                  }, callback);
                    }
                    
                    
                    
                    function callback(response, status) {
                        
                        if (status != google.maps.DistanceMatrixStatus.OK) {
                            alert('Error was: ' + status);
                        } else {
                            var origins = response.originAddresses;
                            var destinations = response.destinationAddresses;
                            var outputDiv = document.getElementById('outputDiv');
                            outputDiv.innerHTML = '';
                            deleteOverlays();
                            
                            var html="";
                            html+='<ul data-role="listview">';
                            
                            
                            for (var i = 0; i < origins.length; i++) {
                                
                                var results = response.rows[i].elements;
                                for (var j = 0; j < results.length; j++) {
                                    
                                    html+='<li><a href="#" storeAddname="'+destinations[j]+'" id="'+origins+'" class="storeLoc" ><h3>'+destinations[j]+'</h3><p class="ui-li-aside"><strong>'+results[j].distance.text+'</strong></p></a></li>';
                                    
                                }
                                
                                html+='</ul>';
                                
                                document.getElementById("outputDiv").innerHTML=html;
                                $("#outputDiv").trigger('create');
                                $.mobile.hidePageLoadingMsg();
                                $("#outputDiv").listview('refresh');
                                
                                
                            }
                        }
                    }
                    
                    
                    function addMarker(location, isDestination) {
                        var icon;
                        if (isDestination) {
                            icon = destinationIcon;
                        } else {
                            icon = originIcon;
                        }
                        geocoder.geocode({'address': location}, function(results, status) {
                                         
                                         if (status == google.maps.GeocoderStatus.OK) {
                                         
                                         bounds.extend(results[0].geometry.location);
                                         map.fitBounds(bounds);
                                         var marker = new google.maps.Marker({
                                                                             map: map,
                                                                             position: results[0].geometry.location,
                                                                             icon: icon
                                                                             });
                                         markersArray.push(marker);
                                         } else {
                                         alert('Geocode was not successful for the following reason: '
                                               + status);
                                         }
                                         });
                    } 
                    
                    function deleteOverlays() {
                        if (markersArray) {
                            for (i in markersArray) {
                                markersArray[i].setMap(null);
                            }
                            markersArray.length = 0;
                        }
                    }  
                    
                    $(document).delegate('.storeLoc', 'click',function(){
                                         
                                         var storeAdd = $(this).attr('storeAddname');
                                         var userAdd = $(this).attr('id');	
                                         
                                         window.sessionStorage.setItem('storeAddress',storeAdd);
                                         window.sessionStorage.setItem('userAddress', userAdd);
                                         //localStorage.setItem('storeAddress', storeAdd);
                                         //localStorage.setItem('userAddress', userAdd);
                                         $.mobile.changePage( "store_map.html", { transition: "slide"} ); 
                                        return false;
                                         
                                         });


    function ajaxCallFailed(){
    
        window.location = "Error.html";
        return false;
    
    
    }

/* Script for Store map Page */

 var userLocation;
                    var storeLocation;
                    $(document).delegate("#storeMap","pageshow",function() {
                                         
                                         $.mobile.showPageLoadingMsg();
                                         
                                         userLocation = sessionStorage.getItem("userAddress");
                                         storeLocation = sessionStorage.getItem('storeAddress');
                                         loadMap ({
                                                  address: storeLocation,
                                                  idDiv: 'mapContainer',
                                                  zoom: 15,
                                                  mapType: google.maps.MapTypeId.ROADMAP,
                                                  onError: function (error) {
                                                  $('#mapContainer').html (error);
                                                  }
                                                  });
                                         });
                    
                    
                    
                    function loadMap (options) {
                        
                        // geocoder docs: http://code.google.com/apis/maps/documentation/geocoding/
                        
                        var geocoder = new google.maps.Geocoder();
                        
                        if (!geocoder) {
                            if (options.onError)
                            options.onError ('Failed to create geocoder');
                            return;
                        }
                        
                        geocoder.geocode ({
                                          address: options.address
                                          },
                                          function (results, status) {
                                          if (status != google.maps.GeocoderStatus.OK) {
                                          if (options.onError)
                                          options.onError ('Geocoder response failed.');
                                          return;
                                          }
                                          
                                          if (!results.length) {
                                          if (options.onError)
                                          options.onError ('Failed to locate address.');
                                          return;
                                          }
                                          
                                          var myOptions = {
                                          zoom: options.zoom,
                                          center: results[0].geometry.location,
                                          mapTypeId: options.mapType
                                          };
                                          
                                          var map = new google.maps.Map (document.getElementById (options.idDiv), myOptions);
                                          var marker = new google.maps.Marker ({
                                                                               position: results[0].geometry.location,
                                                                               map: map
                                                                               });
                                          $.mobile.hidePageLoadingMsg();
                                          }
                                          );
                    }

